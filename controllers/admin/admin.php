<?php
/**
 * User: tom
 * Date: 31/01/2017
 */

namespace Controllers\Admin;

use Controllers\Base;
use Core\System;
use Models\UserAdmin;
abstract class Admin extends Base {          //should be abstract
    protected $page_title = '';
    protected $page_content = '';
    protected $page_menuActive = [];
    protected $isLoggedIn = false;

    public function __construct () {
        parent::__construct();
        $name = join('', array_slice(explode('\\', strtolower(get_class($this))), -1));     //name of executed class without namespace
        $this->page_menuActive[$name] = 'active';
        $user = UserAdmin::getInstance();
        $this->isLoggedIn = $user->login_check();
    }

    public function build () {
        $html = System::template('admin/main.php', [
            'page_title' => $this->page_title,
            'page_content' => $this->page_content,
            'menuActive' => $this->page_menuActive,
            'auth' => $this->isLoggedIn
        ]);

        return $html;
    }
}