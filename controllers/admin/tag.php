<?php
/**
 * User: tom
 * Date: 05/02/2017
 */

namespace Controllers\Admin;

use Models\Tag as mTag;
use Core\System;
use Core\Exceptions\Error404;
class Tag extends Admin {
    public function __construct () {
        parent::__construct();
        if (!$this->isLoggedIn) {
            header('Location: '.ROOT.'admin/panel/login');
            exit();
        }
    }

    public function action_list () {
        $tag = mTag::getInstance();

        $tagList = $tag->getAll();
        $msg = $_SESSION['msg'] ?? null;
        unset($_SESSION['msg']);

        $this->page_title = 'Tag list';
        $this->page_content = System::template('admin/tag/list.php', [
            'list' => $tagList,
            'msg' => $msg
        ]);
    }

    //TODO re-write add and edit methods with validation through out the the validation class

    public function action_add() {
        $tag = mTag::getInstance();

        if(isset($_POST['save'])){
            $name = htmlspecialchars(trim($_POST['name']));

            if(!$name) {
                $msg = 'Name can\'t be empty';
            } else {
                $lastID = $tag->add([
                    'name' => $name
                ]);
                header('Location: '.ROOT.'admin/tag/list');
                $_SESSION['msg'] = 'The tag was added to the blog under the id = '.$lastID;
                exit();
            }
        } elseif (isset($_POST['cancel'])) {
            unset($_SESSION['msg']);
            header('Location: '.ROOT.'admin/tag/list');
            exit();
        } else {
            $msg = 'Please fill the name of new tag.';
            $name = '';
        }

        $this->page_title = 'New tag';
        $this->page_content = System::template('admin/tag/edit.php', [
            'name' => $name,
            'msg' => $msg
        ]);
    }

    public function action_edit() {
        $tag = mTag::getInstance();

        $id = (int)$this->attr[2] ?? null;

        $result = $tag->getByID($id);

        if ($result == false) {
            throw new Error404('tag with id = '.$id.' doesn\'t exist');
        } else {
            $name = $result['name'];
            $msg = 'Please edit name of the tag';
        }

        if (isset($_POST['save'])) {
            $id = (int)$this->attr[2] ?? null;

            $name = htmlspecialchars(trim($_POST['name']));

            if (!$name) {
                $msg = 'Please fill the name';
            } else {
                $tag->editByID($id, [
                    'name' => $name
                ]);
                header('Location: ' . ROOT . 'admin/tag/list');
                $_SESSION['msg'] = 'The tag was updated.';
                exit();
            }
        } elseif (isset($_POST['cancel'])) {
            unset($_SESSION['msg']);
            header('Location: ' . ROOT . 'admin/tag/list');
            exit();
        }

        $this->page_title = 'Edit tag';
        $this->page_content = System::template('admin/tag/edit.php', [
            'name' => $name,
            'msg' => $msg
        ]);
    }

    public function action_delete() {
        $tag = mTag::getInstance();

        $id = (int)$this->attr[2] ?? null;
        $result = $tag->getByID($id);

        if($result == false) {
            throw new Error404('tag with id = '.$id.' doesn\'t exist');
        } else {
            $tag->deleteByID($id);
            header('Location: '.ROOT.'admin/tag/list');
            $_SESSION['msg'] = 'The tag was deleted from the blog.';
            exit();
        }
    }
}