<?php
/**
 * User: tom
 * Date: 18/02/2017
 */

namespace Controllers\Admin;

use Core\System;
use Models\UserAdmin;
class Auth extends Admin {
    public function action_login () {
        if (count($_POST) > 0) {
            $login = (isset($_POST['login'])) ? trim($_POST['login']) : '';
            $password = (isset($_POST['password'])) ? md5(trim($_POST['password'])) : '';

            $user = UserAdmin::getInstance();

            if($user->login_check(['login' => $login, 'password' => $password])) {
                $_SESSION['auth'] = true;

                if (isset($_POST['remember'])) {
                    setcookie('login', $login, time() + 3600 * 24 * 7, '/admin/');
                    setcookie('password', $password, time() + 3600 * 24 * 7, '/admin/');
                }
                header('Location: '.ROOT.'admin/panel/index');
                exit();
            } else {
                $msg = 'Incorrect password/login.';
            }
        }

        $this->page_title = 'Login';
        $this->page_content = System::template('admin/login.php', [
            'msg' => $msg ?? null
        ]);
    }

    public function action_logout () {
        $_SESSION['auth'] = false;
        setcookie('login', '', 1, '/admin/');
        setcookie('password', '', 1, '/admin/');
        unset($_COOKIE['login']);
        unset($_COOKIE['password']);
        unset($_GET['user']);
        $this->action_login();
        header('Location: '.ROOT.'admin/auth/login');
        exit();
    }
}