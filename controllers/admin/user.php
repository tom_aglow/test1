<?php
/**
 * User: tom
 * Date: 05/02/2017
 */

namespace Controllers\Admin;


class User extends Admin {
    public function __construct () {
        parent::__construct();
        if (!$this->isLoggedIn) {
            header('Location: '.ROOT.'admin/panel/login');
            exit();
        }
    }
}