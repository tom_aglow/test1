<?php
/**
 * User: tom
 * Date: 04/02/2017
 */

namespace Controllers\Admin;

use Core\System;
class Panel extends Admin{

    public function action_index() {
        if (!$this->isLoggedIn) {
            header('Location: '.ROOT.'admin/auth/login');
            exit();
        } else {
            $this->page_title = 'Admin panel';
            $this->page_content = System::template('admin/panel.php', [
                //no variables for content
            ]);
        }
    }

    public function action_error404 () {
        if ($this->isLoggedIn) {
            $this->page_title = '404';
            $this->page_content = System::template('admin/404.php', [
                //no variables for content
            ]);
        } else {
            header('Location: '.ROOT.'admin/auth/login');
            exit();
        }
    }

    public function action_error503 () {
        if ($this->isLoggedIn) {
            $this->page_title = '503';
            $this->page_content = System::template('admin/503.php', [
                //no variables for content
            ]);
        } else {
            header('Location: '.ROOT.'admin/auth/login');
            exit();
        }
    }
}