<?php
/**
 * User: tom
 * Date: 04/02/2017
 */

namespace Controllers\Admin;

use Models\Article as mArticle;
use Core\System;
use Core\Validation;
use Core\Exceptions\Error404;
class Article extends Admin{
    public function __construct () {
        parent::__construct();
        if (!$this->isLoggedIn) {
            header('Location: '.ROOT.'admin/auth/login');
            exit();
        }
    }

    public function action_list() {
        $article = mArticle::getInstance();

        $articleList = $article->getAll();
        $msg = $_SESSION['msg'] ?? null;
        unset($_SESSION['msg']);

        $this->page_title = 'Article list';
        $this->page_content = System::template('admin/article/list.php', [
            'list' => $articleList,
            'msg' => $msg
        ]);
    }

    public function action_add() {
        $article = mArticle::getInstance();

        if(isset($_POST['save'])){
            $valid = (new Validation($_POST, $article))->execute();

            if(!$valid->isValidated()) {
                $msg = implode('<br>', $valid->getErrors());
                extract($valid->getPreparedObject());
            } else {
                $lastID = $article->add(array_merge(
                    ['author' => 'admin'],
                    $valid->getPreparedObject()
                ));
                header('Location: '.ROOT.'admin/article/list');
                $_SESSION['msg'] = 'The article was added to the blog under the id = '.$lastID;
                exit();
            }
        } elseif (isset($_POST['cancel'])) {
            unset($_SESSION['msg']);
            header('Location: '.ROOT.'admin/article/list');
            exit();
        } else {
            $msg = 'Please fill the title and the text of new article.';
            $title = '';
            $content = '';
        }

        $this->page_title = 'New article';
        $this->page_content = System::template('admin/article/edit.php', [
            'title' => $title,
            'content' => $content,
            'msg' => $msg
        ]);
    }

    public function action_delete() {
        $article = mArticle::getInstance();

        $id = (int)$this->attr[2] ?? null;
        $result = $article->getByID($id);

        if($result == false) {
            throw new Error404('article with id = '.$id.' doesn\'t exist');
        } else {
            $article->deleteByID($id);
            header('Location: '.ROOT.'admin/article/list');
            $_SESSION['msg'] = 'The article was deleted from the blog.';
            exit();
        }
    }

    public function action_edit() {
        $article = mArticle::getInstance();

        $id = (int)$this->attr[2] ?? null;

        $result = $article->getByID($id);

        if($result == false) {
            throw new Error404('article with id = '.$id.' doesn\'t exist');
        } else {
            $title = $result['title'];
            $content = $result['content'];
            $msg = 'Please edit content or title';
        }

        if(isset($_POST['save'])) {
            $_POST['edited_id'] = $id;
            $valid = (new Validation($_POST, $article))->execute();

            if (!$valid->isValidated()) {
                $msg = implode('<br>', $valid->getErrors());
                extract($valid->getPreparedObject());
            } else {
                $article->editByID($id, $valid->getPreparedObject());
                header('Location: '.ROOT.'admin/article/list');
                $_SESSION['msg'] = 'The article was updated.';
                exit();
            }
        } elseif (isset($_POST['cancel'])) {
            unset($_SESSION['msg']);
            header('Location: '.ROOT.'admin/article/list');
            exit();
        }

        $this->page_title = 'Edit article';
        $this->page_content = System::template('admin/article/edit.php', [
            'title' => $title,
            'content' => $content,
            'msg' => $msg
        ]);
    }
}