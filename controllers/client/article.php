<?php

namespace Controllers\Client;

use Core\Exceptions\Error404;
use Core\System;
use Models\Article as mArticle;
use Models\Tag As mTag;
class Article extends Client {
    public function action_index() {
        $article = mArticle::getInstance();

        $articleList = $article->getAll();

        $this->page_title = 'Index';
        $this->page_content = System::template('client/list.php', [
            'list' => $articleList
        ]);
    }

    public function action_show() {
        $article = mArticle::getInstance();

        $result = $article->getByID($this->attr[2]);

        if($result == false) {                             // check whether the article exist
            throw new Error404('article with id = '.$this->attr[2].' doesn\'t exist');
        }

        $tags = mTag::getInstance()->getByArticleID($result['id_article']);

        $this->page_title = 'Article';
        $this->page_content = System::template('client/article.php', [
            'title' => $result['title'],
            'content' => $result['content'],
            'tag_list' => $tags
        ]);
    }
}