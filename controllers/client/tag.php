<?php
/**
 * User: tom
 * Date: 12/02/2017
 */

namespace Controllers\Client;

use Core\Exceptions\Error404;
use Core\System;
use Models\Article as mArticle;
use Models\Tag As mTag;
class Tag extends Client{
    public function action_show() {
        $articleList = mArticle::getInstance()->getByTagID($this->attr[2]);
        $tag = mTag::getInstance()->getByID($this->attr[2]);

        if($articleList == false || $tag == false) {
            throw new Error404('there is no such tag with id = '.$this->attr[2]);       //TODO rewrite conditional for tag->article bunch
        }

        $this->page_title = 'All '.$tag['name'];
        $this->page_content = System::template('client/list.php', [
            'list' => $articleList
        ]);
    }
}