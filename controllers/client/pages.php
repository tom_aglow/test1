<?php
/**
 * User: tom
 * Date: 31/01/2017
 */

namespace Controllers\Client;

use Core\System;
class Pages extends Client {
    public function action_contacts() {
        $this->page_title = 'Contacts';
        $this->page_content = System::template('client/contacts.php', [
            //no variables for content
        ]);
    }

    public function action_error404 () {
        $this->page_title = '404 Not found';
        $this->page_content = System::template('client/404.php', [
            //no variables for content
        ]);
    }

    public function action_error503 () {
        //Service Unavailable
        $this->page_title = '503 Service Unavailable';
        $this->page_content = System::template('client/503.php', [
            //no variables for content
        ]);
    }
}