<?php
/**
 * User: tom
 * Date: 31/01/2017
 */

namespace Controllers\Client;

use Core\Exceptions\Critical;
use Core\System;
use Controllers\Base;
use Models\Tag;
abstract class Client extends Base {
    protected $page_title;
    protected $page_content;
    protected $attr;
    protected $tag_list;

    public function __construct () {
        parent::__construct();
        $this->page_title = 'Index';
        $this->page_content = '';
        try {
            $this->tag_list = Tag::getInstance()->getAll();
        } catch (Critical $e) {
            $this->tag_list = [];
        }
    }

    public function build () {
        $html = System::template('client/main.php', [
            'page_title' => $this->page_title,
            'page_content' => $this->page_content,
            'tag_list' => $this->tag_list
        ]);

        return $html;
    }
}