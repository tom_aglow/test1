<?php

namespace Controllers;

use Core\Exceptions\Error404;
abstract class Base {

    public abstract function build ();

    public function __construct () {

    }

    public function __call ($name, $arguments) {
        throw new Error404("undefined action _ $name _");
    }

    public function getURLAttributes ($attr) {
        $this->attr = $attr;
    }
}