<?php
/**
 * User: tom
 * Date: 18/02/2017
 */

namespace Core;

class Validation {
    protected $object;
    protected $preparedObject;
    protected $rules;
    protected $errors = [];
    protected $entity;

    public function __construct ($object, $entity) {
        $this->object = $object;
        $this->rules = $entity->getValidationMap();
        $this->entity = $entity;
    }

    public function execute () {
        foreach ($this->object as $index => $value) {
            $value = trim($value);

            if (in_array($index, $this->rules['not_empty']) && $value == '') {
                $this->errors[] = "Field _{$index}_ can't be empty";
            } elseif ($this->checkMinLength($index, $value)) {
                //nothing to do, error array is filled in the method
            } elseif ($this->isRecordExist($index, $value)) {
                $this->errors[] = "The record with _{$index}_ = _{$value}_ is already exist";
            }

            if (in_array($index, $this->rules['fields'])) {
                $this->preparedObject[$index] = (in_array($index, $this->rules['html_allowed'])) ? $value : htmlspecialchars($value);
            }
        }

        return $this;
    }

    public function isValidated () {
        return empty($this->errors);
    }

    public function getPreparedObject () {
        return $this->preparedObject;
    }

    public function getErrors () {
        return $this->errors;
    }

    private function checkMinLength ($column, $value) {
        if (isset($this->rules['min_length'][$column]) && mb_strlen($value, ENCODING) < $this->rules['min_length'][$column]) {      //mb_strlen - if use different languages in the site
            $this->errors[] = "Field _{$column}_ can't be less than {$this->rules['min_length'][$column]}";
            return true;
        } else {
            return false;
        }
    }

    private function isRecordExist ($column, $value) {
        if (in_array($column, $this->rules['unique'])) {
            $table = $this->entity->table;
            $primaryKey = $this->entity->primaryKey;
            if (isset($this->object['edited_id'])) {        //whether record is being edited or created
                $id = $this->object['edited_id'];
                $res = SQL::getInstance()->select("SELECT * FROM {$table} WHERE {$column} = :col AND {$primaryKey} <> :primaryKey", [
                    'col' => $value,
                    'primaryKey' => $id
                ]);
            } else {
                $res = SQL::getInstance()->select("SELECT * FROM {$table} WHERE {$column} = :col", [
                    'col' => $value
                ]);
            }

            return (count($res) > 0) ? true : false;
        } else {
            return false;
        }
    }
}