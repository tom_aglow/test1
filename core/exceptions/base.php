<?php
/**
 * User: tom
 * Date: 23/02/2017
 */

namespace Core\Exceptions;

use Exception;
abstract class Base extends Exception{
    public $logPath = PATH_LOG;
    public function __construct ($message = "", $code = 0, Exception $previous = null) {
        parent::__construct($message, $code, $previous);
        $msg = "\n" . date("H:i:m") . "\n\n" .
            $_SERVER['REMOTE_ADDR'] . ': ' . $_SERVER['REQUEST_METHOD'] . ' => ' . $_SERVER['REQUEST_URI'] . "\n" .
            $this .
            "\n----------------------------------------";
        file_put_contents($this->logPath . date("Y-m-d"), $msg, FILE_APPEND);
    }
}