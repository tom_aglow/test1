<?php
/**
 * User: tom
 * Date: 23/02/2017
 */

namespace Core\Exceptions;


class Error404 extends Base {
    public function __construct ($message = "", $code = 0, Exception $previous = null) {
        $this->logPath .= 'error404/';
        parent::__construct($message, $code, $previous);
    }
}