<?php
/**
 * User: tom
 * Date: 23/02/2017
 */

namespace Core\Exceptions;

class Critical extends Base {
    public function __construct ($message = "", $code = 0, Exception $previous = null) {
        $this->logPath .= 'critical/';
        parent::__construct($message, $code, $previous);
        //send alert to admin's e-mail
    }
}