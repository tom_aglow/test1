<?php

define('WORKMODE', 'dev');      // 'dev' or 'prod'

if (WORKMODE == 'dev') {
    define('ROOT', '/php_webinar_s2/blog/');
} elseif (WORKMODE == 'prod') {
    define('ROOT', '/');
}

define('PATH_LOG', 'logs/');
define('ENCODING', 'UTF-8');