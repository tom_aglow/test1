<?php
/**
 * User: tom
 * Date: 11/02/2017
 */

namespace Models;

use Core\Exceptions\Error404;
use Core\SQL;
abstract class Base {
    protected $db;
    protected $table;
    protected $primaryKey;

    public abstract function getValidationMap ();

    protected function __construct () {
        $this->db = SQL::getInstance();
    }

    public function __get ($name) {
        return $this->$name;
    }

    public function getByID ($id) {
        $res = $this->db->select("SELECT * FROM {$this->table} WHERE {$this->primaryKey} = :primaryKey", [
            'primaryKey' => $id
        ]);
        return $res[0] ?? null;
    }

    public function getByValue ($column, $value) {
        $res = $this->db->select("SELECT * FROM {$this->table} WHERE {$column} = :cKey", [
            'cKey' => $value
        ]);
        return $res[0] ?? null;
    }

    public function getAll ($order = null) {
        return $this->db->select("SELECT * FROM {$this->table} ".$order);
    }

    public function deleteByID ($id) {
        return $this->db->delete($this->table, "{$this->primaryKey} = :primaryKey", ['primaryKey' => $id]);
    }

    public function add ($params) {
        $map = $this->getValidationMap();

        foreach ($params as $index => $value) {
            if (!in_array($index, $map['fields'])) {
                throw new Error404("There isn'\t field _{$index}_ in the table __{$this->table}__");
                //change exception type
            }
        }

        return $this->db->insert($this->table, $params);
    }

    public function editByID ($id, $params) {
        return $this->db->update($this->table, $params, "{$this->primaryKey} = :primaryKey", ['primaryKey' => $id]);
    }
}