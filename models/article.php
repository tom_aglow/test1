<?php

namespace Models;

class Article extends Base {
    use \Core\Traits\Singleton;

    protected function __construct () {
        parent::__construct();
        $this->table = 'articles';
        $this->primaryKey = 'id_article';
    }

    public function getAll ($order = null) {
        return parent::getAll('ORDER BY pub_date DESC');
    }

    public function getByTagID ($id) {
        $sql = 'SELECT * FROM tags_articles LEFT JOIN articles USING (id_article) WHERE id_tag = :id_tag ';
        $params = ['id_tag' => $id];
        return $this->db->select($sql, $params);
    }

    public function getValidationMap () {
        //TODO need tool to change the pub_date of article

        return [
            'fields' => ['author', 'pub_date', 'title', 'content'],
            'unique' => ['title'],
            'not_empty' => ['author', 'title', 'content'],
            'min_length' => ['title' => 5, 'content' => 20],
            'html_allowed' => ['content'],
            'rexec' => [                                                //if needed match with regular expression
                'title' => '/^def/'
            ]
        ];
    }
}

