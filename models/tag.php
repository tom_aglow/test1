<?php
/**
 * User: tom
 * Date: 11/02/2017
 */

namespace Models;

class Tag extends Base {
    use \Core\Traits\Singleton;

    protected function __construct () {
        parent::__construct();
        $this->table = 'tags';
        $this->primaryKey = 'id_tag';
    }

    public function getAll ($order = null) {
        return parent::getAll('ORDER BY name ASC');
    }

    public function getByArticleID ($id) {
        $sql = 'SELECT tags_articles.id_tag, tags.name FROM tags_articles LEFT JOIN tags USING (id_tag) WHERE id_article = :id_article ';
        $params = ['id_article' => $id];
        return $this->db->select($sql, $params);
    }

    public function getValidationMap () {
        return [
            'fields' => ['name'],
            'not_empty' => ['name'],
            'min_length' => ['title' => 3]
        ];
    }
}