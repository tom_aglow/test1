<?php
session_start();

require_once ('core/config.php');
use \Core\Exceptions\Error404;
use \Core\Exceptions\Critical;

/* ------autoload classes------*/

spl_autoload_register(function ($className) {
    $className = strtolower($className);
    $className = str_replace('\\', '/', $className);
    if (file_exists($className . '.php')) {
        include_once($className . '.php');
    } else {
        throw new Error404("controller _ $className _ doesn't exist");
    }
});

/* ------get params from url------*/

$url_query = explode('/', $_GET['querystring']);
if ($url_query[count($url_query) - 1] === '') {
    unset($url_query[count($url_query) - 1]);
}

if (isset($url_query[0]) && $url_query[0] == 'admin') {
    unset($url_query[0]);
    $url_query = array_values($url_query);
    $siteSide = 'admin';
    $cName = isset($url_query[0]) ? $url_query[0] : 'panel';      //default controller is 'panel'
} else {
    $siteSide = 'client';
    $cName = isset($url_query[0]) ? $url_query[0] : 'article';      //default controller is 'article'
}

$cName = 'controllers\\'.$siteSide.'\\'.ucfirst($cName);
$action = isset($url_query[1]) ? 'action_'.$url_query[1] : 'action_index';


/* -----run controller------*/

try {
    $controller = new $cName();

// check if the controller is a child of BASE controller (get_parent_class();)
// double check: all controllers should be children of BASE controller
    if (get_parent_class($controller) != 'Controllers\Client\Client' && get_parent_class($controller) != 'Controllers\Admin\Admin') {
        throw new Error404("called controller _ $controller _ is not a child of parent controller");
    }

    $controller->getURLAttributes($url_query);     //also could include $_GET[] elements
    $controller->$action();
    echo $controller->build();

} catch (Error404 $e) {
    if (true) {
        echo "<pre>";
        print_r($e->getMessage().'<br>'.$e->getTraceAsString());
        echo "</pre>";
    }

    switch ($siteSide) {
        case 'admin':
            $controller = new \Controllers\Admin\Panel();
            break;
        case 'client':
            $controller = new \Controllers\Client\Pages();
            break;
    }
    $action = 'action_error404';

    $controller->getURLAttributes($url_query);     //also could include $_GET[] elements
    $controller->$action();
    echo $controller->build();

} catch (Critical $e) {
    if (true) {
        echo "<pre>";
        print_r($e->getMessage().'<br>'.$e->getTraceAsString());
        echo "</pre>";
    }

    switch ($siteSide) {
        case 'admin':
            $controller = new \Controllers\Admin\Panel();
            break;
        case 'client':
            $controller = new \Controllers\Client\Pages();
            break;
    }
    $action = 'action_error503';

    $controller->getURLAttributes($url_query);     //also could include $_GET[] elements
    $controller->$action();
    echo $controller->build();
}




