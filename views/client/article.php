<h1><?= $title ?></h1>
<? foreach ($tag_list as $item): ?>
<a href="<?= ROOT?>tag/show/<?= $item['id_tag']?>" class="a_tag"><span class="label label-default"><?= $item['name']?></span></a>
<? endforeach; ?>
<br><br>
<p class="text"><?= nl2br($content); ?></p>