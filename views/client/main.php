<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= ROOT?>styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="<?= ROOT?>script.js"></script>
    <title><?= $page_title?></title>
</head>
<body>
    <header class="navbar navbar-default">
        <div class="container">
            <span class="navbar-brand text_logo">Lorem Ipsum</span>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="<?= ROOT?>">all articles</a></li>
<!--                <li class="dropdown">-->
<!--                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">by categories <span class="caret"></span></a>-->
<!--                    <ul class="dropdown-menu">-->
<!--                        <li><a href="#">news</a></li>-->
<!--                        <li><a href="#">travelling</a></li>-->
<!--                        <li><a href="#">private life</a></li>-->
<!--                        <li role="separator" class="divider"></li>-->
<!--                        <li><a href="#">for latest month</a></li>-->
<!--                    </ul>-->
<!--                </li>-->
                <li><a href="<?= ROOT?>pages/contacts">contacts</a></li>
            </ul>
        </div>
    </header>
    <section class="container">
        <ol class="breadcrumb">
            <li><a href="<?= ROOT?>">Home</a></li>
            <li><a href="#">Library</a></li>
            <li class="active">Data</li>
        </ol>
    </section>

    <section class="container">
        <div class="col-md-9">
            <?= $page_content ?>
        </div>
        <? if(count($tag_list) > 0) :?>
        <div class="col-md-3">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Tags</h3>
                </div>
                <div class="panel-body">
                <? foreach ($tag_list as $item): ?>
                    <a href="<?= ROOT?>tag/show/<?= $item['id_tag']?>" class="a_tag"><span class="label label-info"><?= $item['name']?></span></a>
                <? endforeach; ?>
                </div>
            </div>
        </div>
        <?endif; ?>
    </section>
<!--    <footer class="navbar navbar-default">-->
<!--        <div class="container">-->
<!--            <p class="navbar-text pull-left">Created by tom minty</p>-->
<!--        </div>-->
<!--    </footer>-->

</body>
</html>