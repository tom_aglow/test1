<? foreach($list as $row) : ?>
<div class="media">
    <div class="media-left">
        <a href="<?= ROOT?>article/show/<?=$row['id_article']?>">
            <img class="media-object" src="<?= ROOT?>views/client/img/placeholder.svg" alt="article">
        </a>
    </div>
    <div class="media-body">
        <h4 class="media-heading"><?=$row['title']?></h4>
        <p><?=substr($row['content'], 0, 600)?>...</p>
        <div class="btn-read"><a href="<?= ROOT?>article/show/<?=$row['id_article']?>" class="btn btn-primary btn-sm" role="button">Read more</a></div>
    </div>
</div>
<? endforeach; ?>
