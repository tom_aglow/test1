<address class="col-md-8">
    <strong>Ohhhh, Inc.</strong><br>
    1355 Market Street, Suite 900<br>
    San Francisco, CA 94103<br>
    <span class="glyphicon glyphicon-earphone"></span> (123) 456-7890
    <br><br>
    <strong>Tom Minty</strong><br>
    <a href="mailto:#">tom@ohhhh.me</a>
</address>