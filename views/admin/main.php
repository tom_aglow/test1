<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= ROOT?>styles.css">
    <script src="<?= ROOT?>script.js"></script>
    <title><?= $page_title?></title>
</head>
<body>
<header class="navbar navbar-inverse">
    <div class="container">
        <span class="navbar-brand text_logo">Lorem Ipsum</span>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="<?= ROOT?>">go to the site</a></li>
            <li><a href="<?= ROOT?>admin/auth/logout">logout</a></li>
        </ul>
    </div>
</header>
<section class="container">
    <? if ($auth): ?>
    <aside class="col-md-2">
        <div class="list-group">
            <a href="<?= ROOT?>admin/article/list" class="list-group-item <?= $menuActive['article'] ?? ''?>">Articles</a>
            <a href="<?= ROOT?>admin/tag/list" class="list-group-item <?= $menuActive['tag'] ?? ''?>">Tags</a>
            <a href="<?= ROOT?>admin/user" class="list-group-item <?= $menuActive['user'] ?? ''?>">Users</a>
        </div>
    </aside>
    <? endif; ?>
<!--    --><?//= var_dump($menuActive)?>
    <?= $page_content ?>
</section>
</body>
</html>