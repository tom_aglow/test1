<section class="col-md-10">
    <form method="post" class="form-horizontal">
        <div class="form-group">
            <label id="title" class="col-sm-1 control-label">Title</label>
            <div class="col-md-8">
                <input class="form-control" type="text" id="title" name="title" value="<?= $title; ?>">
            </div>
        </div>
        <div class="form-group">
            <label id="content" class="col-sm-1 control-label">Text</label>
            <div class="col-md-8">
                <textarea class="form-control" name="content" id="content" rows="18"><?= $content; ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-1 col-md-2">
                <input class="form-control btn btn-success" type="submit" value="Save" name="save">
            </div>
            <div class="col-md-2">
                <input class="form-control btn btn-default" type="submit" value="Cancel" name="cancel">
            </div>
        </div>
    </form><br>
    <div class="col-sm-offset-1 alert alert-warning col-md-8">
        <?= $msg; ?>
    </div>
</section>

