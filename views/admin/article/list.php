<section class="col-md-7">
    <table class="table table-striped ">
        <tr>
            <th>Date</th>
            <th colspan="3">Article title</th>
        </tr>
        <? foreach($list as $row) :
            $f_date = date('Y-m-d', strtotime($row['pub_date'])); ?>
            <tr>
                <td class="t-col-md"><?=$f_date?></td>
                <td><?=$row['title']?></td>
                <td class="t-col-sm"><a class="btn btn-xs btn-primary" href="<?= ROOT?>admin/article/edit/<?=$row['id_article']?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                <td class="t-col-sm"><a class="btn btn-xs btn-danger" href="<?= ROOT?>admin/article/delete/<?=$row['id_article']?>" onclick="return confirmDelete();"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>
            </tr>
        <? endforeach; ?>
    </table>
    <a class="btn btn-sm btn-info" href="<?= ROOT?>admin/article/add">+add</a><br>
    <? if(isset($msg) && !empty($msg)): ?>
    <div class="alert alert-info col-md-8 article_msg">
        <?=$msg ?>
    </div>
    <? endif; ?>
</section>


