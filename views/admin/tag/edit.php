<section class="col-md-6">
    <form method="post" class="form-horizontal">
        <div class="form-group">
            <label id="name" class="col-sm-2 control-label">Name</label>
            <div class="col-md-6">
                <input class="form-control" type="text" id="name" name="name" value="<?= $name; ?>" placeholder="#tag">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-md-3">
                <input class="form-control btn btn-success" type="submit" value="Save" name="save">
            </div>
            <div class="col-md-3">
                <input class="form-control btn btn-default" type="submit" value="Cancel" name="cancel">
            </div>
        </div>
    </form><br>
    <div class="col-sm-offset-2 alert alert-warning col-md-6">
        <?= $msg; ?>
    </div>
</section>

