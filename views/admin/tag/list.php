<section class="col-md-10">
    <div class="row">
        <? foreach($list as $row) :?>
            <div class="tag">
                <h4><span class="label label-default tag_name"><?=$row['name']?></span></h4>
                <a class="btn btn-xs btn-primary" href="<?= ROOT?>admin/tag/edit/<?=$row['id_tag']?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                <a class="btn btn-xs btn-danger" href="<?= ROOT?>admin/tag/delete/<?=$row['id_tag']?>" onclick="return confirmDelete();"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
            </div>
        <? endforeach; ?>
    </div><br>
    <div class="row">
        <a class="btn btn-sm btn-info" href="<?= ROOT?>admin/tag/add">+add</a><br>
    </div>
    <? if(isset($msg) && !empty($msg)): ?>
        <div class="alert alert-info col-md-8 article_msg">
            <?=$msg ?>
        </div>
    <? endif; ?>
</section>


